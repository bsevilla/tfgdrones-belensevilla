﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateWing : MonoBehaviour
{
    public GameObject wing;
    int sentido;
    void Start()
    {
        if (wing.name == "Ala1" || wing.name == "Ala3")
        {
            sentido = -1;
        }
        else if (wing.name == "Ala2" || wing.name == "Ala4")
        {
            sentido = 1;
        }
    }
    void Update()
    {
        //wing = GameObject.FindWithTag("Wing");
        wing.transform.Rotate(0f, sentido * 10f, 0f, Space.Self);
    }
}
