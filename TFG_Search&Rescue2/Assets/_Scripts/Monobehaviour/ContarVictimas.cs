﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContarVictimas : MonoBehaviour
{
    public GameObject buscador;
    private Vector3 posicionActual;
    private RaycastHit hit;
    public int numeroDeVictimas = 1;

    public float range = 100f;
    private Ray shootRay;

    public float distancia;

    void Update()
    {
        
        if (distancia < 100f)
        {
            RestaVictima();
        }
    }
    public GameObject objetivo;
    public string tagBuscable;

    public GameObject BuscaObjetivoMasCercano(GameObject buscador)
    {
        GameObject[] listaBuscable = GameObject.FindGameObjectsWithTag(tagBuscable);
        float distanciaMax = 5000;

        if (listaBuscable.Length > 0)
        {

            foreach (GameObject cosa in listaBuscable)
            {
                distancia = (buscador.transform.position - cosa.transform.position).sqrMagnitude;
                if (distancia < distanciaMax)
                {
                    distanciaMax = distancia;
                    objetivo = cosa;
                }
            }
        }
        else
        {

            objetivo = null;

        }

        return objetivo;
    }

    void RestaVictima()
    {
        posicionActual = buscador.transform.position;
        Vector3 down = new Vector3(posicionActual.x, 0f, posicionActual.z);
        shootRay.origin = posicionActual;
        shootRay.direction = objetivo.transform.position - posicionActual;
        Debug.Log(shootRay);

        if (Physics.Raycast(shootRay, out hit, range))
        {

            Debug.DrawRay(posicionActual, shootRay.direction * hit.distance, Color.yellow);
            if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Victim"))
            {
                numeroDeVictimas -= 1;
            }
                //numeroDeVictimas -= 1;
                if (numeroDeVictimas == 0)
            {
                Debug.Log("Todas las víctimas encontradas");
            }
            Debug.Log("Lanzado el rayo");
        }
        
    }
}
