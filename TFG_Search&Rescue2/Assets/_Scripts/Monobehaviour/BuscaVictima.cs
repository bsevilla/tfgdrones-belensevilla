﻿using System;
using UnityEngine;
using UnityEngine.AI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class BuscaVictima : MonoBehaviour
{
    public NavMeshAgent dronAgent;
    public GameObject droneGO;
    //public GameObject objetivo;

    public EstadosDron estado;
    private RaycastHit hit;

    private Vector3 movdirection;
    private Vector3 posicionActual;
    private Vector3 punto45grados;
    public Vector3 NewPos;
    private Vector3 altura;
    public Vector3 possibleMove;
    public Vector3 actualMove;

    private float TiempoGradiente = 0.5f;
    private float cornerAngle;

    public int noGradiente = 0;
    private int vuelta = 0;

    private bool sube = false;

    
    void Awake()
    {
        droneGO = GameObject.FindWithTag("Drone");
        dronAgent = GetComponent<NavMeshAgent>();
        
        CaminaRandom();
    }

    void FixedUpdate()
    {
        Movement();
        posicionActual = droneGO.transform.position;
    }


    void Movement()
    {

        TiempoGradiente -= Time.deltaTime;
        if (TiempoGradiente <= 0)
        {
            //Debug.Log("Buscando Objetivo");
            //BuscaObjetivo Controlador = GetComponent<BuscaObjetivo>();
            //Controlador.BuscaObjetivoMasCercano(droneGO);
            BuscaObjetivoMasCercano(droneGO);

            if(objetivo != null)
            {
                dronAgent.SetDestination(objetivo.transform.position);
                //if (Physics.Raycast(posicionActual, Vector3.down, out hit, LayerMask.GetMask("Fake")))
                //{
                //    objetivo = null;
                //    Debug.Log("Elegir Camino");
                //    ElegirCamino();
                //    estado = EstadosDron.falsaAlarma;
                //}
            }
            else
            {
                //Debug.Log("Elegir Camino");
                ElegirCamino(); //Intenta caminar por máxima pendiente y si no la encuentra anda aleatoriamente
            }
            
            TiempoGradiente = 10f;
            vuelta = 0;
        }
    }
    public GameObject objetivo;
    public string tagBuscable;
    public float distancia = 1000f;

    public GameObject BuscaObjetivoMasCercano(GameObject buscador)
    {
        GameObject[] listaBuscable = GameObject.FindGameObjectsWithTag(tagBuscable);
        float distanciaMax = 5000;

        if (listaBuscable.Length > 0)
        {

            foreach (GameObject cosa in listaBuscable)
            {
                distancia = (buscador.transform.position - cosa.transform.position).sqrMagnitude;
                if (distancia < distanciaMax)
                {
                    distanciaMax = distancia;
                    objetivo = cosa;
                }
            }
        }
        else
        {

            objetivo = null;

        }

        return objetivo;
    }

    void ElegirCamino()
    {
        for (vuelta = 0; vuelta < 8; vuelta++)
        {
            cornerAngle = (Mathf.PI / 4) * vuelta;
            //Debug.Log("Buscando Gradiente");
            BuscaGradiente(cornerAngle);
            AlturaActual();
            // Debug.DrawLine(origen, final, color, tiempo visible, false);
            Debug.DrawLine(posicionActual, possibleMove, Color.blue, 1f);
            noGradiente += 1;
        }

        if(noGradiente >= 8)
        {
            if (actualMove.y > altura.y)
            {
                //Debug.Log("Subiendo");
                Sube();
            }
            else
            {
                for (vuelta = 0; vuelta < 8; vuelta++)
                {
                    //Debug.Log("Camino Random");
                    CaminaRandom();
                }
                    
            }
            noGradiente = 0;
        }
    }

    void BuscaGradiente(float cornerAngle)
    {
        estado = EstadosDron.buscandoGradiente;
        punto45grados.x = Mathf.Sin(cornerAngle);
        punto45grados.z = Mathf.Cos(cornerAngle);
        movdirection = new Vector3(punto45grados.x, -1f, punto45grados.z);
        // raycast(origen, dirección, máxima distancia, layer)
        
        if (Physics.Raycast(posicionActual, movdirection, out hit))
        {
            if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Walkable"))
            {
                possibleMove = hit.point;
                if (possibleMove.y > actualMove.y)
                    actualMove = possibleMove;
            }
        }
    }
    
    void AlturaActual()
    {
        if (Physics.Raycast(posicionActual, Vector3.down, out hit, LayerMask.GetMask("Walkable")))
        {
            altura = hit.point;
        }
    }

    void Sube()
    {
        sube = true;
        TiempoGradiente += 2f;
        dronAgent.SetDestination(actualMove);
        dronAgent.speed = 5;
    }

    void CaminaRandom()
    {
        estado = EstadosDron.caminandoRandom;
        sube = false;
        TiempoGradiente += 2f;
        NewPos.x = UnityEngine.Random.onUnitSphere.x * 200;
        NewPos.z = UnityEngine.Random.onUnitSphere.z * 200;

        dronAgent.SetDestination(NewPos);
        dronAgent.speed = 5;
    }
}

public enum EstadosDron
{
    buscandoGradiente,
    caminandoRandom,
    comprobandoPosibleVictima,
    falsaAlarma,
    victimaDetectada
}

//[Serializable]
//class PosicionesGuardar
//{
//    public Vector3 LastPosition;

//    public PosicionesGuardar(Vector3 LastPosition)
//    {
//        this.LastPosition=LastPosition;
//    }
//}
public class BuscaObjetivo : MonoBehaviour
{
    

}



