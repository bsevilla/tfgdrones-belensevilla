﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara3persona : MonoBehaviour
{
    public GameObject dron;
    private Vector3 distancia;

    // Start is called before the first frame update
    void Start()
    {
        distancia = transform.position - dron.transform.position;    
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = dron.transform.position + distancia;
    }
}
