﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CreateGameObjects : MonoBehaviour

{
    public GameObject entityToSpawn;

    public CreateVictims spawnManagerValues;

    int instanceNumber = 1;

    void Start()
    {
        SpawnEntities();
    }

    void SpawnEntities()
    {
        int currentSpawnPointIndex = 0;

        for (int i = 0; i < (spawnManagerValues.numberOfPrefabsToCreate - 1); i++)
        {

            GameObject currentEntity = Instantiate(entityToSpawn, spawnManagerValues.spawnPoints[currentSpawnPointIndex], Quaternion.identity);

            currentEntity.name = "Victima" + instanceNumber;

            instanceNumber++;
            currentSpawnPointIndex++;
        }
    }
}