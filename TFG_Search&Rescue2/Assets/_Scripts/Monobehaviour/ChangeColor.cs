﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    public GameObject drone;
    //public List<GameObject> listaDrones;
    public Texture2D texture;

    public Color color1;
    public Color color2;
    public Color color3;
    public Color degradado;

    private Vector3 pos;

    public int numberOfDrones;
    private int x, y;
    private int xB, yB;

    void Start()
    {
        drone = GameObject.FindWithTag("Drone");
        texture = new Texture2D(500, 500);
        GetComponent<Renderer>().material.mainTexture = texture;
        color1 = new Color(0.145f, 0.145f, 0.145f, 1.0f);
        color2 = new Color(0.568f, 0.568f, 0.568f, 0.5f);
        color3 = new Color(0.945f, 0.945f, 0.945f, 0.1f);
        degradado = Color.white;
        //FirstPosition();
    }

    void FirstPosition()
    {
        pos = drone.transform.position;
        xB = (int)pos.x;
        yB = (int)pos.z;
        texture.SetPixel(xB, yB, Color.blue);
        texture.Apply();
        Debug.Log(x); Debug.Log(y);
    }

    void Update()
    {
        Draw();
        degradado = Color.Lerp(Color.white, Color.black, Mathf.PingPong(Time.time, 1));
    }

    private int radio1 = 10;
    private int radio2 = 25;
    private int radio3 = 40;
    void Draw()
    {
        pos = drone.transform.position;
        x = (int)pos.x;
        y = (int)pos.z;

        {
            if (x != xB || y != yB)
            {
                for (int j = x - radio3; j < x + radio3; j++)
                {
                    degradado = Color.Lerp(Color.white, Color.black, Mathf.PingPong(Time.time, 1));
                    //if ((j > x - radio3 && j < x - radio2) || (j > x + radio2 && j < x + radio3)) 
                    //{
                    //    texture.SetPixel(j, y, color1);
                    //    texture.Apply();
                    //    Debug.Log("color1");
                    //}

                    //else if ((j > x - radio2 && j < x - radio1) || (j > x + radio1 && j < x + radio2))
                    //{
                    //    texture.SetPixel(j, y, color2);
                    //    texture.Apply();
                    //    Debug.Log("color2");
                    //}
                    //else if (j > x - radio1 && j < x + radio1)
                    //{
                    //    texture.SetPixel(j, y, color3);
                    //    texture.Apply();
                    //    Debug.Log("color3");
                    //}
                    texture.SetPixel(x, j, degradado);
                    texture.Apply();
                    Debug.Log(degradado);
                }
                xB = x;
                yB = y;
            }
        }
    }
}
