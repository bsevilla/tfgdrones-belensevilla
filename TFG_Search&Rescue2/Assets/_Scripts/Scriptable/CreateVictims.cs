﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Puntos Objetivo", order = 3)]

public class CreateVictims : ScriptableObject

{
    public int numberOfPrefabsToCreate;
    public List<Vector3> spawnPoints;
    public Terrain terrain;
    void OnEnable()
    {

        spawnPoints = new List<Vector3>();

        for (int i = 1; i < numberOfPrefabsToCreate; i++)
        {
            Vector3 posicionNueva = new Vector3(Random.Range(0f, 500f), 200f, Random.Range(0f, 500f));
            spawnPoints.Add(posicionNueva);
        }
    }
}